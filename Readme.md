# Process for exporting:
* Go [here](https://kibana.atlassian.io/app/kibana#/settings/objects)
* Search for `jvs.dev.api` (etc.)
* Export each of the Dashboards, Searches and Visualizations into a separate file in the appropriate folder.
* Run `./fix-ordering.sh`, then commit.
    + This script is needed because Kibana produces JSON blobs in non-deterministic order. You can add a git hook for it with `cp fix-ordering.sh .git/hooks/pre-commit`.

# Process for PRs
* Merge the PR
* Delete all the objects in the JSON blob you are about to import (if you don't, you will need to click Overwrite ~40 times)
* Import the changed blobs (same URL as above)

