#!/bin/bash

# Requires jq

find . -iname '*.json' | while read file; do
    cat "$file" | jq sort > "$file.new"
    mv "$file.new" "$file"
    git add "$file"
done

